#!/bin/bash

# When not limiting the open file descritors limit, the memory consumption of
# slapd is absurdly high. See https://github.com/docker/docker/issues/8231
ulimit -n 8192

mkdir -p /etc/ldap/slapd.d

exec /usr/local/libexec/slapd -f /etc/ldap.conf -F /etc/ldap/slapd.d -d 1