FROM alpine:3.5

RUN apk add --no-cache alpine-sdk gcc g++ make groff libtool

WORKDIR /ldap

COPY . /ldap
RUN ./configure \
    --enable-debug \
    --enable-syslog \
    --enable-slapd=yes \
    --enable-rwm=yes \
    --enable-meta=yes \
    --enable-ldap=yes \
    --enable-modules=yes \
    --enable-accesslog \
    --enable-auditlog \
    --enable-collect \
    --enable-memberof \
    --enable-syncprov \
    && make depend \
    && make \
    && make install \
    && rm -rf /ldap 

WORKDIR /

COPY entry.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

EXPOSE 389

VOLUME ["/etc/ldap", "/var/lib/ldap"]

ENTRYPOINT ["/entrypoint.sh"]
